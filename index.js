var express = require('express');
var app = express();

var passport = require('passport');
var session = require('express-session');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: '54c497d2-f4fd-11e7-8c3f-9a214cf093ae'
}));

app.use(passport.initialize());
app.use(passport.session());

var MySQL = require('./app/config/MySQL');
var Sequelize = require('sequelize');

var sequelize = new Sequelize(
  MySQL.connection.database,
  MySQL.connection.user,
  MySQL.connection.password, {
    dialect: MySQL.dialect,
    host: MySQL.connection.host,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    operatorsAliases: false
});

sequelize.authenticate().then(() => {
  console.log('Connection has been established successfully.');

}).catch(err => {
  console.error('Unable to connect to the database:', err);
});

const User = sequelize.define('user', {
  confirm: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  confirmed: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      is: /^[a-z0-9\_\-]+$/i
    }
  },
  email: {
    type: Sequelize.STRING,
    validate: {
      isEmail: true
    }
  },
  firstName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  }
});

User.sync({force: false}).then(() => {
  console.log('User table created!');
});

app.post('/', function(req, res) {

});

app.post('/register', function(req, res) {

});

app.post('/login', function(req, res) {

});

app.post('/logout', function(req, res) {

});

app.post('/confirm', function(req, res) {
  User.update({ confirmed: true }, {
    where: { confirm: req.body.confirm }

  }).then(result => {
    User.findOne({ where: {confirm: req.body.confirm} }).then(user => {
      if (user.confirmed === true) {
        return res.status(200).json({
          message: 'user confirmed',
        });

      } else {
        return res.status(400).json({
          message: 'user not confirmed',
        });
      }
    });

  }).catch(err =>{
    return res.status(400).json({
      message: err.message,
    });
  })
});

app.listen(3000, function(err) {
  if (!err) {
    console.log("Server running @ http://localhost:3000");

  } else { console.log(err); }
});