module.exports = {
  dialect: 'mysql',
  connection: {
    host: '127.0.0.1',
    user: 'YOUR_DB_USERNAME_HERE',
    password: 'YOUR_DB_PASSWORD_HERE',
    database: 'YOUR_DB_NAME_HERE'
  }
};